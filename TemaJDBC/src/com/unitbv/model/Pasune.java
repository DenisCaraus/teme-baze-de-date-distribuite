package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pasune database table.
 * 
 */
@Entity
@NamedQuery(name="Pasune.findAll", query="SELECT p FROM Pasune p")
public class Pasune implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_pasune")
	private int idPasune;

	private int arie;

	//bi-directional many-to-one association to Animal
	@OneToMany(mappedBy="pasuneBean")
	private List<Animal> animals;

	//bi-directional many-to-one association to Muncitor
	@OneToMany(mappedBy="pasuneBean")
	private List<Muncitor> muncitors;

	//bi-directional many-to-one association to Pepasune
	@OneToMany(mappedBy="pasuneBean")
	private List<Pepasune> pepasunes;

	public Pasune() {
	}

	public Pasune(int idPasune, int arie) {
		super();
		this.idPasune = idPasune;
		this.arie = arie;
	}

	public int getIdPasune() {
		return this.idPasune;
	}

	public void setIdPasune(int idPasune) {
		this.idPasune = idPasune;
	}

	public int getArie() {
		return this.arie;
	}

	public void setArie(int arie) {
		this.arie = arie;
	}

	public List<Animal> getAnimals() {
		return this.animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}

	public Animal addAnimal(Animal animal) {
		getAnimals().add(animal);
		animal.setPasuneBean(this);

		return animal;
	}

	public Animal removeAnimal(Animal animal) {
		getAnimals().remove(animal);
		animal.setPasuneBean(null);

		return animal;
	}

	public List<Muncitor> getMuncitors() {
		return this.muncitors;
	}

	public void setMuncitors(List<Muncitor> muncitors) {
		this.muncitors = muncitors;
	}

	public Muncitor addMuncitor(Muncitor muncitor) {
		getMuncitors().add(muncitor);
		muncitor.setPasuneBean(this);

		return muncitor;
	}

	public Muncitor removeMuncitor(Muncitor muncitor) {
		getMuncitors().remove(muncitor);
		muncitor.setPasuneBean(null);

		return muncitor;
	}

	public List<Pepasune> getPepasunes() {
		return this.pepasunes;
	}

	public void setPepasunes(List<Pepasune> pepasunes) {
		this.pepasunes = pepasunes;
	}

	public Pepasune addPepasune(Pepasune pepasune) {
		getPepasunes().add(pepasune);
		pepasune.setPasuneBean(this);

		return pepasune;
	}

	public Pepasune removePepasune(Pepasune pepasune) {
		getPepasunes().remove(pepasune);
		pepasune.setPasuneBean(null);

		return pepasune;
	}

	@Override
	public String toString() {
		return "Pasune [idPasune = " + idPasune + ", arie = " + arie + "]";
	}

}