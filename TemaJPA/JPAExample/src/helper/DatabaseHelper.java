package helper;

import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DatabaseHelper {

	private DatabaseHelper() {

	}

	private static DatabaseHelper instance;

	public static DatabaseHelper getInstance() {
		if (instance == null) {
			instance = new DatabaseHelper();
			instance.init();
		}

		return instance;
	}

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Executes a specific transaction
	 * 
	 * @param action The action that is to be executed
	 * @return True if the operation was successful, false otherwise
	 */
	public boolean executeTransaction(Consumer<EntityManager> action) {
		EntityTransaction entityTransaction = entityManager.getTransaction();
		try {
			entityTransaction.begin();
			action.accept(entityManager);
			entityTransaction.commit();
		} catch (RuntimeException e) {
			System.err.println("Transaction error: " + e.getLocalizedMessage());
			entityTransaction.rollback();
			return false;
		}

		return true;

	}

	/**
	 * Initializes the DatabaseManager
	 * 
	 * @return True if the operation was successful, false otherwise
	 */
	private boolean init() {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("JPAExample");
			entityManager = entityManagerFactory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Error at initialing DatabaseManager: " + e.getMessage().toString());
			return false;
		}

		return true;
	}
}
