package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the muncitor database table.
 * 
 */
@Entity
@NamedQuery(name="Muncitor.findAll", query="SELECT m FROM Muncitor m")
public class Muncitor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String cnp;

	@Column(name="nume_prenume")
	private String numePrenume;

	private int salariu;

	private String telefon;

	//bi-directional many-to-one association to Grajd
	@ManyToOne
	@JoinColumn(name="grajd")
	private Grajd grajdBean;

	//bi-directional many-to-one association to Pasune
	@ManyToOne
	@JoinColumn(name="pasune")
	private Pasune pasuneBean;

	//bi-directional many-to-one association to Terenarabil
	@ManyToOne
	@JoinColumn(name="teren")
	private Terenarabil terenarabil;

	public Muncitor() {
	}

	public Muncitor(String cnp, String numePrenume, String telefon, int salariu, Terenarabil terenarabil,
			Pasune pasuneBean, Grajd grajdBean) {
		super();
		this.cnp = cnp;
		this.numePrenume = numePrenume;
		this.telefon = telefon;
		this.salariu = salariu;
		this.terenarabil = terenarabil;
		this.pasuneBean = pasuneBean;
		this.grajdBean = grajdBean;
	}

	public String getCnp() {
		return this.cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getNumePrenume() {
		return this.numePrenume;
	}

	public void setNumePrenume(String numePrenume) {
		this.numePrenume = numePrenume;
	}

	public int getSalariu() {
		return this.salariu;
	}

	public void setSalariu(int salariu) {
		this.salariu = salariu;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public Grajd getGrajdBean() {
		return this.grajdBean;
	}

	public void setGrajdBean(Grajd grajdBean) {
		this.grajdBean = grajdBean;
	}

	public Pasune getPasuneBean() {
		return this.pasuneBean;
	}

	public void setPasuneBean(Pasune pasuneBean) {
		this.pasuneBean = pasuneBean;
	}

	public Terenarabil getTerenarabil() {
		return this.terenarabil;
	}

	public void setTerenarabil(Terenarabil terenarabil) {
		this.terenarabil = terenarabil;
	}

	@Override
	public String toString() {
		return "Muncitor [cnp = " + cnp + ", numePrenume = " + numePrenume + ", salariu = " + salariu + ", telefon = " + telefon
				+ ", grajdBean = " + grajdBean + ", pasuneBean = " + pasuneBean + ", terenarabil = " + terenarabil + "]";
	}

}