package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the pegrajd database table.
 * 
 */
@Entity
@NamedQuery(name = "PeGrajd.findAll", query = "SELECT p FROM PeGrajd p")
public class PeGrajd implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	// bi-directional many-to-one association to Grajd
	@ManyToOne
	@JoinColumn(name = "grajd")
	private Grajd grajdBean;

	// bi-directional many-to-one association to Resurse
	@ManyToOne
	@JoinColumn(name = "resursa")
	private Resurse resurse;

	public PeGrajd() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Grajd getGrajdBean() {
		return this.grajdBean;
	}

	public void setGrajdBean(Grajd grajdBean) {
		this.grajdBean = grajdBean;
	}

	public Resurse getResurse() {
		return this.resurse;
	}

	public void setResurse(Resurse resurse) {
		this.resurse = resurse;
	}

	@Override
	public String toString() {
		return "Pegrajd [id = " + id + ", grajdBean = " + grajdBean + ", resurse = " + resurse + "]";
	}
}