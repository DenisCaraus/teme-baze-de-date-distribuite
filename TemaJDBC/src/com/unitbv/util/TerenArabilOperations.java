package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Terenarabil;

public class TerenArabilOperations {
	private DatabaseConnection databaseConnection;

	public TerenArabilOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all TerenArabil entries from the database
	 * 
	 * @return The TerenArabil list
	 */
	public List<Terenarabil> getAllTeren() {
		databaseConnection.createConnection();
		String query = "SELECT idTeren, dataCulegere, cantitatePlantata, arieArabila FROM terenarabil";
		List<Terenarabil> terens = new ArrayList<Terenarabil>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Terenarabil teren = new Terenarabil();

				teren.setIdTeren(rs.getInt("idTeren"));
				teren.setCantitatePlantata(rs.getInt("cantitatePlantata"));
				teren.setArie(rs.getInt("arieArabila"));
				teren.setDataCulegere(rs.getDate("dataCulegere"));

				terens.add(teren);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return terens;
	}

	/**
	 * Adds a new TerenArabil entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addTeren(Terenarabil teren) {
		databaseConnection.createConnection();
		String query = "INSERT INTO terenarabil VALUES (?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, teren.getIdTeren());
			ps.setDate(2, new java.sql.Date(teren.getDataCulegere().getTime()));
			ps.setInt(3, teren.getCantitatePlantata());
			ps.setInt(4, teren.getArie());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an TerenArabil entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateTeren(Terenarabil teren) {
		databaseConnection.createConnection();
		String query = "UPDATE terenarabil SET dataCulegere = ?, cantitatePlantata = ?, arieArabila = ? WHERE idTeren = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(4, teren.getIdTeren());
			ps.setDate(1, new java.sql.Date(teren.getDataCulegere().getTime()));
			ps.setInt(2, teren.getCantitatePlantata());
			ps.setInt(3, teren.getArie());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an TerenArabil entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteTeren(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM teren where idTeren = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
