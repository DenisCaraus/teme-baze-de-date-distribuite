package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the grajd database table.
 * 
 */
@Entity
@NamedQuery(name = "Grajd.findAll", query = "SELECT g FROM Grajd g")
public class Grajd implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idGrajd;

	private int capacitateMaxima;

	// bi-directional many-to-one association to Animal
	@OneToMany(mappedBy = "grajdBean")
	private List<Animal> animals;

	// bi-directional many-to-one association to Muncitor
	@OneToMany(mappedBy = "grajdBean")
	private List<Muncitor> muncitors;

	// bi-directional many-to-one association to PeGrajd
	@OneToMany(mappedBy = "grajdBean")
	private List<PeGrajd> pegrajds;

	public Grajd() {
	}

	public int getIdGrajd() {
		return this.idGrajd;
	}

	public void setIdGrajd(int idGrajd) {
		this.idGrajd = idGrajd;
	}

	public int getCapacitateMaxima() {
		return this.capacitateMaxima;
	}

	public void setCapacitateMaxima(int capacitateMaxima) {
		this.capacitateMaxima = capacitateMaxima;
	}

	public List<Animal> getAnimals() {
		return this.animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}

	public Animal addAnimal(Animal animal) {
		getAnimals().add(animal);
		animal.setGrajdBean(this);

		return animal;
	}

	public Animal removeAnimal(Animal animal) {
		getAnimals().remove(animal);
		animal.setGrajdBean(null);

		return animal;
	}

	public List<Muncitor> getMuncitors() {
		return this.muncitors;
	}

	public void setMuncitors(List<Muncitor> muncitors) {
		this.muncitors = muncitors;
	}

	public Muncitor addMuncitor(Muncitor muncitor) {
		getMuncitors().add(muncitor);
		muncitor.setGrajdBean(this);

		return muncitor;
	}

	public Muncitor removeMuncitor(Muncitor muncitor) {
		getMuncitors().remove(muncitor);
		muncitor.setGrajdBean(null);

		return muncitor;
	}

	public List<PeGrajd> getPegrajds() {
		return this.pegrajds;
	}

	public void setPegrajds(List<PeGrajd> pegrajds) {
		this.pegrajds = pegrajds;
	}

	public PeGrajd addPegrajd(PeGrajd pegrajd) {
		getPegrajds().add(pegrajd);
		pegrajd.setGrajdBean(this);

		return pegrajd;
	}

	public PeGrajd removePegrajd(PeGrajd pegrajd) {
		getPegrajds().remove(pegrajd);
		pegrajd.setGrajdBean(null);

		return pegrajd;
	}

	@Override
	public String toString() {
		return "Grajd [idGrajd = " + idGrajd + ", capacitateMaxima = " + capacitateMaxima + "]";
	}
}