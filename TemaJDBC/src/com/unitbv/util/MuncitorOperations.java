package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Muncitor;
import com.unitbv.model.Terenarabil;
import com.unitbv.model.Pasune;
import com.unitbv.model.Grajd;

public class MuncitorOperations {
	private DatabaseConnection databaseConnection;

	public MuncitorOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all Muncitor entries from the database
	 * 
	 * @return The Muncitor list
	 */
	public List<Muncitor> getAllMuncitors() {
		databaseConnection.createConnection();
		String query = "SELECT cnp, numePrenume, telefon, salariu, idTeren, dataCulegere, cantitatePlantata, arieArabila, idPasune, arie, idGrajd, capacitateMaxima "
				+ " FROM muncitor LEFT JOIN terenarabil ON muncitor.teren = terenarabil.idTeren LEFT JOIN pasune ON muncitor.pasune = pasune.idPasune LEFT JOIN grajd ON muncitor.grajd = grajd.idGrajd";
		List<Muncitor> muncitors = new ArrayList<Muncitor>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Muncitor muncitor = new Muncitor();
				Terenarabil teren = new Terenarabil();
				Pasune pasune = new Pasune();
				Grajd grajd = new Grajd();

				teren.setIdTeren(rs.getInt("idTeren"));
				teren.setDataCulegere(rs.getDate("dataCulegere"));
				teren.setArie(rs.getInt("arieArabila"));
				teren.setCantitatePlantata(rs.getInt("cantitatePlantata"));

				pasune.setIdPasune(rs.getInt("idPasune"));
				pasune.setArie(rs.getInt("arie"));

				grajd.setIdGrajd(rs.getInt("idGrajd"));
				grajd.setCapacitateMaxima(rs.getInt("capacitateMaxima"));

				muncitor.setCnp(rs.getString("cnp"));
				muncitor.setNumePrenume(rs.getString("numePrenume"));
				muncitor.setTelefon(rs.getString("telefon"));
				muncitor.setSalariu(rs.getInt("salariu"));
				muncitor.setGrajdBean(grajd);
				muncitor.setPasuneBean(pasune);
				muncitor.setTerenarabil(teren);

				muncitors.add(muncitor);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return muncitors;
	}

	/**
	 * Adds a new Muncitor entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addMuncitor(Muncitor muncitor) {
		databaseConnection.createConnection();
		String query = "INSERT INTO muncitor VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setString(1, muncitor.getCnp());
			ps.setString(2, muncitor.getNumePrenume());
			ps.setString(3, muncitor.getTelefon());
			ps.setInt(4, muncitor.getSalariu());
			ps.setInt(5, muncitor.getTerenarabil().getIdTeren());
			ps.setInt(6, muncitor.getPasuneBean().getIdPasune());
			ps.setInt(7, muncitor.getGrajdBean().getIdGrajd());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an Muncitor entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateMuncitor(Muncitor muncitor) {
		databaseConnection.createConnection();
		String query = "UPDATE muncitor SET numePrenume = ?, telefon = ?,  salariu = ?, teren = ?, pasune = ?, grajd = ? WHERE cnp = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setString(7, muncitor.getCnp());
			ps.setString(1, muncitor.getNumePrenume());
			ps.setString(2, muncitor.getTelefon());
			ps.setInt(3, muncitor.getSalariu());
			ps.setInt(4, muncitor.getTerenarabil().getIdTeren());
			ps.setInt(5, muncitor.getPasuneBean().getIdPasune());
			ps.setInt(6, muncitor.getGrajdBean().getIdGrajd());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an Muncitor entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteMuncitor(String cnp) {

		databaseConnection.createConnection();
		String query = "DELETE FROM muncitor where cnp = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setString(1, cnp);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
