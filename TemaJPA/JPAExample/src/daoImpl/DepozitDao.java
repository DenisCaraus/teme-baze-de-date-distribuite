package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Depozit;

/**
 * DAO class for Depozit entity
 * 
 * @author k0bra
 *
 */
public class DepozitDao implements Dao<Depozit> {

	private DatabaseHelper databaseHelper;

	public DepozitDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Depozit> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Depozit.class, id));
	}

	@Override
	public List<Depozit> getAll() {
		TypedQuery<Depozit> query = databaseHelper.getEntityManager().createQuery("SELECT d from Depozit d",
				Depozit.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Depozit depozit) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(depozit));
	}

	@Override
	public boolean update(Depozit old, Depozit newObj) {
		old.setCantitateMaxima(newObj.getCantitateMaxima());
		old.setResurses(newObj.getResurses());
		old.setProduses(newObj.getProduses());
		old.setTipDepozit(newObj.getTipDepozit());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Depozit depozit) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(depozit));
	}

}
