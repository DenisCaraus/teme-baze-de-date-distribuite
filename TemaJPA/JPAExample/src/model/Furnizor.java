package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the furnizor database table.
 * 
 */
@Entity
@NamedQuery(name = "Furnizor.findAll", query = "SELECT f FROM Furnizor f")
public class Furnizor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idFurnizor;

	private String denumire;

	// bi-directional many-to-one association to Resurse
	@OneToMany(mappedBy = "furnizorBean")
	private List<Resurse> resurses;

	public Furnizor() {
	}

	public int getIdFurnizor() {
		return this.idFurnizor;
	}

	public void setIdFurnizor(int idFurnizor) {
		this.idFurnizor = idFurnizor;
	}

	public String getDenumire() {
		return this.denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public List<Resurse> getResurses() {
		return this.resurses;
	}

	public void setResurses(List<Resurse> resurses) {
		this.resurses = resurses;
	}

	public Resurse addResurs(Resurse resurs) {
		getResurses().add(resurs);
		resurs.setFurnizorBean(this);

		return resurs;
	}

	public Resurse removeResurs(Resurse resurs) {
		getResurses().remove(resurs);
		resurs.setFurnizorBean(null);

		return resurs;
	}

	@Override
	public String toString() {
		return "Furnizor [idFurnizor = " + idFurnizor + ", denumire = " + denumire + "]";
	}
}