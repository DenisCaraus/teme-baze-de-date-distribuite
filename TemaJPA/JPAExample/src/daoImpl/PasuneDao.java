package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Pasune;

/**
 * DAO class for Pasune entity
 * 
 * @author k0bra
 *
 */
public class PasuneDao implements Dao<Pasune> {

	private DatabaseHelper databaseHelper;

	public PasuneDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Pasune> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Pasune.class, id));
	}

	@Override
	public List<Pasune> getAll() {
		TypedQuery<Pasune> query = databaseHelper.getEntityManager().createQuery("SELECT p from Pasune p",
				Pasune.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Pasune pasune) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(pasune));
	}

	@Override
	public boolean update(Pasune old, Pasune newObj) {
		old.setAnimals(newObj.getAnimals());
		old.setArie(newObj.getArie());
		old.setMuncitors(newObj.getMuncitors());
		old.setPepasunes(newObj.getPepasunes());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Pasune pasune) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(pasune));
	}

}