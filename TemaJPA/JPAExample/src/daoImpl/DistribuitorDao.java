package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Distribuitor;

/**
 * DAO class for Distribuitor entity
 * 
 * @author k0bra
 *
 */
public class DistribuitorDao implements Dao<Distribuitor> {

	private DatabaseHelper databaseHelper;

	public DistribuitorDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Distribuitor> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Distribuitor.class, id));
	}

	@Override
	public List<Distribuitor> getAll() {
		TypedQuery<Distribuitor> query = databaseHelper.getEntityManager().createQuery("SELECT d from Distribuitor d",
				Distribuitor.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Distribuitor distribuitor) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(distribuitor));
	}

	@Override
	public boolean update(Distribuitor old, Distribuitor newObj) {
		old.setDenumire(newObj.getDenumire());
		old.setProduses(newObj.getProduses());
		old.setSumaMaximaDisponibila(newObj.getSumaMaximaDisponibila());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Distribuitor distribuitor) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(distribuitor));
	}

}
