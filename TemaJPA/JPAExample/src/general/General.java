package general;

import java.util.List;
import java.util.Scanner;

import daoImpl.PasuneDao;
import daoImpl.GrajdDao;
import daoImpl.FurnizorDao;
import helper.DatabaseHelper;
import model.Pasune;
import model.Grajd;
import model.Furnizor;

/**
 * General operations class
 * 
 * @author k0bra
 *
 */
public class General {

	/**
	 * Prints an list of elements
	 * 
	 * @param items
	 */
	public static <Type> void printList(List<Type> items) {
		Integer count = 0;
		for (Type item : items) {
			System.out.println(count.toString() + ". " + item.toString());
			count++;
		}
	}

	/**
	 * Demonstrative menu for this application
	 * 
	 * @param databaseHelper
	 */
	public static void menu(DatabaseHelper databaseHelper) {
		Scanner scan = new Scanner(System.in);
		int selection = 0;
		while (true) {
			System.out.println("Alege entitatea la care vrei sa lucrezi:");
			System.out.println("1.Pasune");
			System.out.println("2.Grajd");
			System.out.println("3.Furnizor");
			System.out.println("4.Exit");

			if (scan.hasNextInt())
				selection = Integer.parseInt(scan.nextLine());

			switch (selection) {
			case 1:
				menuPasune(databaseHelper, scan);
				break;
			case 2:
				menuGrajd(databaseHelper, scan);
				break;
			case 3:
				menuFurnizor(databaseHelper, scan);
				break;
			case 4:
				scan.close();
				return;
			}
		}
	}

	/**
	 * Demonstrative menu for Pasune CRUD
	 * 
	 * @param databaseHelper
	 */
	public static void menuPasune(DatabaseHelper databaseHelper, Scanner scan) {
		PasuneDao dao = new PasuneDao(databaseHelper);

		System.out.println("Alege operatia:");
		System.out.println("1.Create");
		System.out.println("2.Read");
		System.out.println("3.Update");
		System.out.println("4.Delete");

		switch (Integer.parseInt(scan.nextLine())) {
		case 1: {
			int id, arie;
			System.out.println("Introduceti id-ul");
			id = Integer.parseInt(scan.nextLine());
			System.out.println("Introduceti aria");
			arie = Integer.parseInt(scan.nextLine());
			Pasune pasune = new Pasune();
			pasune.setIdPasune(id);
			pasune.setArie(arie);
			dao.create(pasune);
			break;
		}
		case 2: {
			printList(dao.getAll());
			break;
		}
		case 3: {
			List<Pasune> list = dao.getAll();
			printList(list);
			System.out.println("Alegeti itemul dupa numar.");
			Pasune pasune = list.get(Integer.parseInt(scan.nextLine()));
			Pasune old = pasune;
			System.out.println("Introduceti noua arie");
			pasune.setArie(Integer.parseInt(scan.nextLine()));
			dao.update(old, pasune);
			break;
		}
		case 4: {
			List<Pasune> list = dao.getAll();
			printList(list);
			System.out.println("Alegeti id-ul elementului care vreti sa fie sters.");
			dao.delete(dao.get(Integer.parseInt(scan.nextLine())).get());
			break;
		}
		}

	}

	/**
	 * Demonstrative menu for Grajd CRUD
	 * 
	 * @param databaseConnection
	 */
	public static void menuGrajd(DatabaseHelper databaseConnection, Scanner scan) {
		GrajdDao dao = new GrajdDao(databaseConnection);

		System.out.println("Alege operatia:");
		System.out.println("1.Create");
		System.out.println("2.Read");
		System.out.println("3.Update");
		System.out.println("4.Delete");

		switch (Integer.parseInt(scan.nextLine())) {
		case 1: {
			int id, capacitateMaxima;
			System.out.println("Introduceti id-ul");
			id = Integer.parseInt(scan.nextLine());
			System.out.println("Introduceti aria");
			capacitateMaxima = Integer.parseInt(scan.nextLine());
			Grajd grajd = new Grajd();
			grajd.setIdGrajd(id);
			grajd.setCapacitateMaxima(capacitateMaxima);
			dao.create(grajd);
			break;
		}
		case 2: {
			printList(dao.getAll());
			break;
		}
		case 3: {
			List<Grajd> list = dao.getAll();
			printList(list);
			System.out.println("Alegeti itemul dupa numar.");
			Grajd grajd = list.get(Integer.parseInt(scan.nextLine()));
			Grajd old = grajd;
			System.out.println("Introduceti noua capacitate maxima");
			grajd.setCapacitateMaxima(Integer.parseInt(scan.nextLine()));
			dao.update(old, grajd);
			break;
		}
		case 4: {
			List<Grajd> list = dao.getAll();
			printList(list);
			System.out.println("Alegeti id-ul elementului care vreti sa fie sters.");
			dao.delete(dao.get(Integer.parseInt(scan.nextLine())).get());
			break;
		}
		}

	}

	/**
	 * Demonstrative menu for Furnizor CRUD
	 * 
	 * @param databaseConnection
	 */
	public static void menuFurnizor(DatabaseHelper databaseConnection, Scanner scan) {
		FurnizorDao dao = new FurnizorDao(databaseConnection);

		System.out.println("Alege operatia:");
		System.out.println("1.Create");
		System.out.println("2.Read");
		System.out.println("3.Update");
		System.out.println("4.Delete");

		switch (Integer.parseInt(scan.nextLine())) {
		case 1: {
			int id;
			String denumire;
			System.out.println("Introduceti id-ul");
			id = scan.nextInt();
			System.out.println("Introduceti denumirea");
			denumire = scan.next();
			Furnizor furnizor = new Furnizor();
			furnizor.setIdFurnizor(id);
			furnizor.setDenumire(denumire);
			dao.create(furnizor);
			break;
		}
		case 2: {
			printList(dao.getAll());
			break;
		}
		case 3: {
			List<Furnizor> list = dao.getAll();
			printList(list);
			System.out.println("Alegeti itemul dupa numar.");
			Furnizor furnizor = list.get(Integer.parseInt(scan.nextLine()));
			Furnizor old = furnizor;
			System.out.println("Introduceti noua denumire");
			furnizor.setDenumire(scan.nextLine());
			dao.update(old, furnizor);
			break;
		}
		case 4: {
			List<Furnizor> list = dao.getAll();
			printList(list);
			System.out.println("Alegeti id-ul elementului care vreti sa fie sters.");
			dao.delete(dao.get(Integer.parseInt(scan.nextLine())).get());
			break;
		}
		}

	}
}