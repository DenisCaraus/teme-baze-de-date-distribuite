package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Grajd;

public class GrajdOperations {
	private DatabaseConnection databaseConnection;

	public GrajdOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all Grajd entries from the database
	 * 
	 * @return The Grajd list
	 */
	public List<Grajd> getAllGrajd() {
		databaseConnection.createConnection();
		String query = "SELECT idGrajd, capacitateMaxima FROM grajd";
		List<Grajd> grajds = new ArrayList<Grajd>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Grajd grajd = new Grajd();

				grajd.setIdGrajd(rs.getInt("idGrajd"));
				grajd.setCapacitateMaxima(rs.getInt("capacitateMaxima"));

				grajds.add(grajd);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return grajds;
	}

	/**
	 * Adds a new Grajd entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addGrajd(Grajd grajd) {
		databaseConnection.createConnection();
		String query = "INSERT INTO grajd VALUES (?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, grajd.getIdGrajd());
			ps.setInt(2, grajd.getCapacitateMaxima());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an Grajd entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateGrajd(Grajd grajd) {
		databaseConnection.createConnection();
		String query = "UPDATE grajd SET capacitateMaxima = ? WHERE idGrajd = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(2, grajd.getIdGrajd());
			ps.setInt(1, grajd.getCapacitateMaxima());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an Grajd entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteGrajd(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM grajd where idGrajd = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
