package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the terenarabil database table.
 * 
 */
@Entity
@NamedQuery(name="Terenarabil.findAll", query="SELECT t FROM Terenarabil t")
public class Terenarabil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_teren")
	private int idTeren;

	private int arie;

	@Column(name="cantitate_plantata")
	private int cantitatePlantata;

	@Temporal(TemporalType.DATE)
	@Column(name="data_culegere")
	private Date dataCulegere;

	//bi-directional many-to-one association to Muncitor
	@OneToMany(mappedBy="terenarabil")
	private List<Muncitor> muncitors;

	//bi-directional many-to-one association to Peteren
	@OneToMany(mappedBy="terenarabil")
	private List<Peteren> peterens;

	//bi-directional many-to-one association to Produs
	@OneToMany(mappedBy="terenarabil")
	private List<Produs> produses;

	public Terenarabil() {
	}

	public Terenarabil(int idTeren, Date dataCulegere, int cantitatePlantata, int arie) {
		super();
		this.idTeren = idTeren;
		this.dataCulegere = dataCulegere;
		this.cantitatePlantata = cantitatePlantata;
		this.arie = arie;
	}

	public int getIdTeren() {
		return this.idTeren;
	}

	public void setIdTeren(int idTeren) {
		this.idTeren = idTeren;
	}

	public int getArie() {
		return this.arie;
	}

	public void setArie(int arie) {
		this.arie = arie;
	}

	public int getCantitatePlantata() {
		return this.cantitatePlantata;
	}

	public void setCantitatePlantata(int cantitatePlantata) {
		this.cantitatePlantata = cantitatePlantata;
	}

	public Date getDataCulegere() {
		return this.dataCulegere;
	}

	public void setDataCulegere(Date dataCulegere) {
		this.dataCulegere = dataCulegere;
	}

	public List<Muncitor> getMuncitors() {
		return this.muncitors;
	}

	public void setMuncitors(List<Muncitor> muncitors) {
		this.muncitors = muncitors;
	}

	public Muncitor addMuncitor(Muncitor muncitor) {
		getMuncitors().add(muncitor);
		muncitor.setTerenarabil(this);

		return muncitor;
	}

	public Muncitor removeMuncitor(Muncitor muncitor) {
		getMuncitors().remove(muncitor);
		muncitor.setTerenarabil(null);

		return muncitor;
	}

	public List<Peteren> getPeterens() {
		return this.peterens;
	}

	public void setPeterens(List<Peteren> peterens) {
		this.peterens = peterens;
	}

	public Peteren addPeteren(Peteren peteren) {
		getPeterens().add(peteren);
		peteren.setTerenarabil(this);

		return peteren;
	}

	public Peteren removePeteren(Peteren peteren) {
		getPeterens().remove(peteren);
		peteren.setTerenarabil(null);

		return peteren;
	}

	public List<Produs> getProduses() {
		return this.produses;
	}

	public void setProduses(List<Produs> produses) {
		this.produses = produses;
	}

	public Produs addProdus(Produs produs) {
		getProduses().add(produs);
		produs.setTerenarabil(this);

		return produs;
	}

	public Produs removeProdus(Produs produs) {
		getProduses().remove(produs);
		produs.setTerenarabil(null);

		return produs;
	}

	@Override
	public String toString() {
		return "Terenarabil [idTeren = " + idTeren + ", arie = " + arie + ", cantitatePlantata = " + cantitatePlantata
				+ ", dataCulegere = " + dataCulegere + "]";
	}

}