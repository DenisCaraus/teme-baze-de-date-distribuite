package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the peteren database table.
 * 
 */
@Entity
@NamedQuery(name = "PeTeren.findAll", query = "SELECT p FROM PeTeren p")
public class PeTeren implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	// bi-directional many-to-one association to Resurse
	@ManyToOne
	@JoinColumn(name = "resursa")
	private Resurse resurse;

	// bi-directional many-to-one association to TerenArabil
	@ManyToOne
	@JoinColumn(name = "teren")
	private TerenArabil terenarabil;

	public PeTeren() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Resurse getResurse() {
		return this.resurse;
	}

	public void setResurse(Resurse resurse) {
		this.resurse = resurse;
	}

	public TerenArabil getTerenarabil() {
		return this.terenarabil;
	}

	public void setTerenarabil(TerenArabil terenarabil) {
		this.terenarabil = terenarabil;
	}

	@Override
	public String toString() {
		return "Peteren [id = " + id + ", resurse = " + resurse + ", terenarabil = " + terenarabil + "]";
	}
}