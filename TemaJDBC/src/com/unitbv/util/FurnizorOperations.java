package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Furnizor;

public class FurnizorOperations {
	private DatabaseConnection databaseConnection;

	public FurnizorOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all Furnizor entries from the database
	 * 
	 * @return The Furnizor list
	 */
	public List<Furnizor> getAllFurnizors() {
		databaseConnection.createConnection();
		String query = "SELECT idFurnizor, denumire FROM furnizor";
		List<Furnizor> furnizors = new ArrayList<Furnizor>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Furnizor furnizor = new Furnizor();

				furnizor.setIdFurnizor(rs.getInt("idFurnizor"));
				furnizor.setDenumire(rs.getString("denumire"));

				furnizors.add(furnizor);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return furnizors;
	}

	/**
	 * Adds a new Furnizor entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addFurnizor(Furnizor furnizor) {
		databaseConnection.createConnection();
		String query = "INSERT INTO furnizor VALUES (?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, furnizor.getIdFurnizor());
			ps.setString(2, furnizor.getDenumire());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an Furnizor entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateFurnizor(Furnizor furnizor) {
		databaseConnection.createConnection();
		String query = "UPDATE furnizor SET denumire = ? WHERE idFurnizor = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(2, furnizor.getIdFurnizor());
			ps.setString(1, furnizor.getDenumire());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an Furnizor entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteFurnizor(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM furnizor where idFurnizor = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
