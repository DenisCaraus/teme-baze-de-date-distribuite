package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Depozit;
import com.unitbv.model.Furnizor;
import com.unitbv.model.Peteren;
import com.unitbv.model.Resurse;
import com.unitbv.model.Terenarabil;

public class PeTerenOperations {
	private DatabaseConnection databaseConnection;

	public PeTerenOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all PeTeren entries from the database
	 * 
	 * @return The PeTeren list
	 */
	public List<Peteren> getAllPeTeren() {
		databaseConnection.createConnection();
		String query = "SELECT id, idTeren, dataCulegere, cantitatePlantata, arieArabila, idResursa, denumireResursa, cantitate, idDepozit, tipDepozit, cantitateMaxima, idFurnizor, denumire "
		+ " FROM peteren LEFT JOIN terenarabil ON peteren.teren = terenarabil.idTeren LEFT JOIN resurse ON peteren.resursa = resurse.idResursa LEFT JOIN depozit ON resurse.depozit = depozit.idDepozit LEFT JOIN furnizor ON resurse.furnizor = furnizor.idFurnizor";
		List<Peteren> peTerenuri = new ArrayList<Peteren>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Peteren peTeren = new Peteren();
				Terenarabil teren = new Terenarabil();
				Resurse resurse = new Resurse();
				Depozit depozit = new Depozit();
				Furnizor furnizor = new Furnizor();

				teren.setIdTeren(rs.getInt("idTeren"));
				teren.setCantitatePlantata(rs.getInt("cantitatePlantata"));
				teren.setArie(rs.getInt("arieArabila"));
				teren.setDataCulegere(rs.getDate("dataCulegere"));

				depozit.setIdDepozit(rs.getInt("idDepozit"));
				depozit.setTipDepozit(rs.getString("tipDepozit"));
				depozit.setCantitateMaxima(rs.getInt("cantitateMaxima"));

				furnizor.setIdFurnizor(rs.getInt("idFurnizor"));
				furnizor.setDenumire(rs.getString("denumire"));

				resurse.setIdResursa(rs.getInt("idResursa"));
				resurse.setDenumire(rs.getString("denumireResursa"));
				resurse.setCantitate(rs.getInt("cantitate"));
				resurse.setDepozitBean(depozit);
				resurse.setFurnizorBean(furnizor);

				peTeren.setId(rs.getInt("id"));
				peTeren.setTerenarabil(teren);
				peTeren.setResurse(resurse);

				peTerenuri.add(peTeren);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return peTerenuri;
	}

	/**
	 * Adds a new PeTeren entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addPeTeren(Peteren peTeren) {
		databaseConnection.createConnection();
		String query = "INSERT INTO peteren VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, peTeren.getTerenarabil().getIdTeren());
			ps.setInt(2, peTeren.getResurse().getIdResursa());
			ps.setInt(3, peTeren.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an PeTeren entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updatePeTeren(Peteren peTeren) {
		databaseConnection.createConnection();
		String query = "UPDATE peteren SET teren = ?, resursa = ? WHERE id = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, peTeren.getTerenarabil().getIdTeren());
			ps.setInt(2, peTeren.getResurse().getIdResursa());
			ps.setInt(3, peTeren.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an PeTeren entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deletePeTeren(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM peteren where id = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
