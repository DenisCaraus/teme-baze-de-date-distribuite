package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the peteren database table.
 * 
 */
@Entity
@NamedQuery(name="Peteren.findAll", query="SELECT p FROM Peteren p")
public class Peteren implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to Resurse
	@ManyToOne
	@JoinColumn(name="resursa")
	private Resurse resurse;

	//bi-directional many-to-one association to Terenarabil
	@ManyToOne
	@JoinColumn(name="teren")
	private Terenarabil terenarabil;

	public Peteren() {
	}

	public Peteren(int id, Resurse resurse, Terenarabil terenarabil) {
		super();
		this.id = id;
		this.resurse = resurse;
		this.terenarabil = terenarabil;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Resurse getResurse() {
		return this.resurse;
	}

	public void setResurse(Resurse resurse) {
		this.resurse = resurse;
	}

	public Terenarabil getTerenarabil() {
		return this.terenarabil;
	}

	public void setTerenarabil(Terenarabil terenarabil) {
		this.terenarabil = terenarabil;
	}

	@Override
	public String toString() {
		return "Peteren [id = " + id + ", resurse = " + resurse + ", terenarabil = " + terenarabil + "]";
	}

}