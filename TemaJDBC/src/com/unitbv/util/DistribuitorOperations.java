package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Distribuitor;

public class DistribuitorOperations {
	private DatabaseConnection databaseConnection;

	public DistribuitorOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all Distribuitor entries from the database
	 * 
	 * @return The Distribuitor list
	 */
	public List<Distribuitor> getAllDistribuitors() {
		databaseConnection.createConnection();
		String query = "SELECT idDistribuitor, denumire, sumaMaximaDisponibila FROM distribuitor";
		List<Distribuitor> distribuitors = new ArrayList<Distribuitor>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Distribuitor distribuitor = new Distribuitor();

				distribuitor.setIdDistribuitor(rs.getInt("idDistribuitor"));
				distribuitor.setDenumire(rs.getString("denumire"));
				distribuitor.setSumaMaximaDisponibila(rs.getInt("sumaMaximaDisponibila"));

				distribuitors.add(distribuitor);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return distribuitors;
	}

	/**
	 * Adds a new Distribuitor entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addDistribuitor(Distribuitor distribuitor) {
		databaseConnection.createConnection();
		String query = "INSERT INTO distribuitor VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, distribuitor.getIdDistribuitor());
			ps.setString(2, distribuitor.getDenumire());
			ps.setInt(3, distribuitor.getSumaMaximaDisponibila());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an Distribuitor entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateDistribuitor(Distribuitor distribuitor) {
		databaseConnection.createConnection();
		String query = "UPDATE distribuitor SET denumire = ?, sumaMaximaDisponibila = ? WHERE idDistribuitor = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(3, distribuitor.getIdDistribuitor());
			ps.setString(1, distribuitor.getDenumire());
			ps.setInt(2, distribuitor.getSumaMaximaDisponibila());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an Distribuitor entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteDistribuitor(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM distribuitor where idDistribuitor = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
