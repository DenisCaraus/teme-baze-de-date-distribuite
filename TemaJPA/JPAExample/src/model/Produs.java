package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the produs database table.
 * 
 */
@Entity
@NamedQuery(name = "Produs.findAll", query = "SELECT p FROM Produs p")
public class Produs implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idProdus;

	private int cantitate;

	private String denumireProdus;

	private BigDecimal greutateProdus;

	private String tipProdus;

	// bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name = "animal")
	private Animal animalBean;

	// bi-directional many-to-one association to Depozit
	@ManyToOne
	@JoinColumn(name = "depozit")
	private Depozit depozitBean;

	// bi-directional many-to-one association to Distribuitor
	@ManyToOne
	@JoinColumn(name = "distribuitor")
	private Distribuitor distribuitorBean;

	// bi-directional many-to-one association to TerenArabil
	@ManyToOne
	@JoinColumn(name = "teren")
	private TerenArabil terenarabil;

	public Produs() {
	}

	public int getIdProdus() {
		return this.idProdus;
	}

	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}

	public int getCantitate() {
		return this.cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public String getDenumireProdus() {
		return this.denumireProdus;
	}

	public void setDenumireProdus(String denumireProdus) {
		this.denumireProdus = denumireProdus;
	}

	public BigDecimal getGreutateProdus() {
		return this.greutateProdus;
	}

	public void setGreutateProdus(BigDecimal greutateProdus) {
		this.greutateProdus = greutateProdus;
	}

	public String getTipProdus() {
		return this.tipProdus;
	}

	public void setTipProdus(String tipProdus) {
		this.tipProdus = tipProdus;
	}

	public Animal getAnimalBean() {
		return this.animalBean;
	}

	public void setAnimalBean(Animal animalBean) {
		this.animalBean = animalBean;
	}

	public Depozit getDepozitBean() {
		return this.depozitBean;
	}

	public void setDepozitBean(Depozit depozitBean) {
		this.depozitBean = depozitBean;
	}

	public Distribuitor getDistribuitorBean() {
		return this.distribuitorBean;
	}

	public void setDistribuitorBean(Distribuitor distribuitorBean) {
		this.distribuitorBean = distribuitorBean;
	}

	public TerenArabil getTerenarabil() {
		return this.terenarabil;
	}

	public void setTerenarabil(TerenArabil terenarabil) {
		this.terenarabil = terenarabil;
	}

	@Override
	public String toString() {
		return "Produs [idProdus = " + idProdus + ", cantitate = " + cantitate + ", denumire = " + denumireProdus
				+ ", greutate = " + greutateProdus + ", tipProdus = " + tipProdus + ", animalBean = " + animalBean
				+ ", depozitBean = " + depozitBean + ", distribuitorBean = " + distribuitorBean + ", terenarabil = "
				+ terenarabil + "]";
	}
}