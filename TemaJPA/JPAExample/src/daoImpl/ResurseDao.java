package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Resurse;

/**
 * DAO class for Resurse entity
 * 
 * @author k0bra
 *
 */
public class ResurseDao implements Dao<Resurse> {

	private DatabaseHelper databaseHelper;

	public ResurseDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Resurse> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Resurse.class, id));
	}

	@Override
	public List<Resurse> getAll() {
		TypedQuery<Resurse> query = databaseHelper.getEntityManager().createQuery("SELECT r from Resurse r",
				Resurse.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Resurse resurse) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(resurse));
	}

	@Override
	public boolean update(Resurse old, Resurse newObj) {
		old.setCantitate(newObj.getCantitate());
		old.setDenumireResursa(newObj.getDenumireResursa());
		old.setDepozitBean(newObj.getDepozitBean());
		old.setFurnizorBean(newObj.getFurnizorBean());
		old.setPegrajds(newObj.getPegrajds());
		old.setPepasunes(newObj.getPepasunes());
		old.setPeterens(newObj.getPeterens());

		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Resurse resurse) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(resurse));
	}

}