package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the depozit database table.
 * 
 */
@Entity
@NamedQuery(name = "Depozit.findAll", query = "SELECT d FROM Depozit d")
public class Depozit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idDepozit;

	private int cantitateMaxima;

	private String tipDepozit;

	// bi-directional many-to-one association to Produs
	@OneToMany(mappedBy = "depozitBean")
	private List<Produs> produses;

	// bi-directional many-to-one association to Resurse
	@OneToMany(mappedBy = "depozitBean")
	private List<Resurse> resurses;

	public Depozit() {
	}

	public int getIdDepozit() {
		return this.idDepozit;
	}

	public void setIdDepozit(int idDepozit) {
		this.idDepozit = idDepozit;
	}

	public int getCantitateMaxima() {
		return this.cantitateMaxima;
	}

	public void setCantitateMaxima(int cantitateMaxima) {
		this.cantitateMaxima = cantitateMaxima;
	}

	public String getTipDepozit() {
		return this.tipDepozit;
	}

	public void setTipDepozit(String tipDepozit) {
		this.tipDepozit = tipDepozit;
	}

	public List<Produs> getProduses() {
		return this.produses;
	}

	public void setProduses(List<Produs> produses) {
		this.produses = produses;
	}

	public Produs addProdus(Produs produs) {
		getProduses().add(produs);
		produs.setDepozitBean(this);

		return produs;
	}

	public Produs removeProdus(Produs produs) {
		getProduses().remove(produs);
		produs.setDepozitBean(null);

		return produs;
	}

	public List<Resurse> getResurses() {
		return this.resurses;
	}

	public void setResurses(List<Resurse> resurses) {
		this.resurses = resurses;
	}

	public Resurse addResurs(Resurse resurs) {
		getResurses().add(resurs);
		resurs.setDepozitBean(this);

		return resurs;
	}

	public Resurse removeResurs(Resurse resurs) {
		getResurses().remove(resurs);
		resurs.setDepozitBean(null);

		return resurs;
	}

	@Override
	public String toString() {
		return "Depozit [idDepozit = " + idDepozit + ", cantitateMaxima = " + cantitateMaxima + ", tipDepozit = "
				+ tipDepozit + "]";
	}
}