package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Depozit;
import com.unitbv.model.Furnizor;
import com.unitbv.model.Pasune;
import com.unitbv.model.Pepasune;
import com.unitbv.model.Resurse;

public class PePasuneOperations {
	private DatabaseConnection databaseConnection;

	public PePasuneOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all PePasune entries from the database
	 * 
	 * @return The PePasune list
	 */
	public List<Pepasune> getAllPePasune() {
		databaseConnection.createConnection();
		String query = "SELECT id, idPasune, arie, idResursa, denumireResursa, cantitate, idDepozit, tipDepozit, cantitateMaxima, idFurnizor, denumire "
				+ " FROM pepasune LEFT JOIN pasune ON pepasune.pasune = pasune.idPasune LEFT JOIN resurse ON pepasune.resursa = resurse.idResursa LEFT JOIN depozit ON resurse.depozit = depozit.idDepozit LEFT JOIN furnizor ON resurse.furnizor = furnizor.idFurnizor";
		List<Pepasune> pePasuni = new ArrayList<Pepasune>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Pepasune pePasune = new Pepasune();
				Pasune pasune = new Pasune();
				Resurse resurse = new Resurse();
				Depozit depozit = new Depozit();
				Furnizor furnizor = new Furnizor();

				pasune.setIdPasune(rs.getInt("idPasune"));
				pasune.setArie(rs.getInt("arie"));

				depozit.setIdDepozit(rs.getInt("idDepozit"));
				depozit.setTipDepozit(rs.getString("tipDepozit"));
				depozit.setCantitateMaxima(rs.getInt("cantitateMaxima"));

				furnizor.setIdFurnizor(rs.getInt("idFurnizor"));
				furnizor.setDenumire(rs.getString("denumire"));

				resurse.setIdResursa(rs.getInt("idResursa"));
				resurse.setDenumire(rs.getString("denumireResursa"));
				resurse.setCantitate(rs.getInt("cantitate"));
				resurse.setDepozitBean(depozit);
				resurse.setFurnizorBean(furnizor);

				pePasune.setId(rs.getInt("id"));
				pePasune.setPasuneBean(pasune);
				pePasune.setResurse(resurse);

				pePasuni.add(pePasune);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return pePasuni;
	}

	/**
	 * Adds a new PePasune entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addPePasune(Pepasune pePasune) {
		databaseConnection.createConnection();
		String query = "INSERT INTO pepasune VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, pePasune.getPasuneBean().getIdPasune());
			ps.setInt(2, pePasune.getResurse().getIdResursa());
			ps.setInt(3, pePasune.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an PePasune entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updatePePasune(Pepasune pePasune) {
		databaseConnection.createConnection();
		String query = "UPDATE pepasune SET pasune = ?, resursa = ? WHERE id = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, pePasune.getPasuneBean().getIdPasune());
			ps.setInt(2, pePasune.getResurse().getIdResursa());
			ps.setInt(3, pePasune.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an PePasune entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deletePePasune(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM pepasune where id = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
