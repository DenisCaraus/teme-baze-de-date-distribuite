package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name = "Animal.findAll", query = "SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idAnimal;

	private BigDecimal greutate;

	private String specie;

	private String tipAnimal;

	private int varsta;

	// bi-directional many-to-one association to Grajd
	@ManyToOne
	@JoinColumn(name = "grajd")
	private Grajd grajdBean;

	// bi-directional many-to-one association to Pasune
	@ManyToOne
	@JoinColumn(name = "pasune")
	private Pasune pasuneBean;

	// bi-directional many-to-one association to Produs
	@OneToMany(mappedBy = "animalBean")
	private List<Produs> produses;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public BigDecimal getGreutate() {
		return this.greutate;
	}

	public void setGreutate(BigDecimal greutate) {
		this.greutate = greutate;
	}

	public String getSpecie() {
		return this.specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public String getTipAnimal() {
		return this.tipAnimal;
	}

	public void setTipAnimal(String tipAnimal) {
		this.tipAnimal = tipAnimal;
	}

	public int getVarsta() {
		return this.varsta;
	}

	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}

	public Grajd getGrajdBean() {
		return this.grajdBean;
	}

	public void setGrajdBean(Grajd grajdBean) {
		this.grajdBean = grajdBean;
	}

	public Pasune getPasuneBean() {
		return this.pasuneBean;
	}

	public void setPasuneBean(Pasune pasuneBean) {
		this.pasuneBean = pasuneBean;
	}

	public List<Produs> getProduses() {
		return this.produses;
	}

	public void setProduses(List<Produs> produses) {
		this.produses = produses;
	}

	public Produs addProdus(Produs produs) {
		getProduses().add(produs);
		produs.setAnimalBean(this);

		return produs;
	}

	public Produs removeProdus(Produs produs) {
		getProduses().remove(produs);
		produs.setAnimalBean(null);

		return produs;
	}

	@Override
	public String toString() {
		return "Animal [idAnimal = " + idAnimal + ", greutate = " + greutate + ", specie = " + specie + ", tipAnimal = "
				+ tipAnimal + ", varsta = " + varsta + ", grajdBean = " + grajdBean + ", pasuneBean = " + pasuneBean
				+ "]";
	}
}