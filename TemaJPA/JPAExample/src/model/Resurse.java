package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the resurse database table.
 * 
 */
@Entity
@NamedQuery(name = "Resurse.findAll", query = "SELECT r FROM Resurse r")
public class Resurse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idResursa;

	private int cantitate;

	private String denumireResursa;

	// bi-directional many-to-one association to PeGrajd
	@OneToMany(mappedBy = "resurse")
	private List<PeGrajd> pegrajds;

	// bi-directional many-to-one association to PePasune
	@OneToMany(mappedBy = "resurse")
	private List<PePasune> pepasunes;

	// bi-directional many-to-one association to PeTeren
	@OneToMany(mappedBy = "resurse")
	private List<PeTeren> peterens;

	// bi-directional many-to-one association to Depozit
	@ManyToOne
	@JoinColumn(name = "depozit")
	private Depozit depozitBean;

	// bi-directional many-to-one association to Furnizor
	@ManyToOne
	@JoinColumn(name = "furnizor")
	private Furnizor furnizorBean;

	public Resurse() {
	}

	public int getIdResursa() {
		return this.idResursa;
	}

	public void setIdResursa(int idResursa) {
		this.idResursa = idResursa;
	}

	public int getCantitate() {
		return this.cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public String getDenumireResursa() {
		return this.denumireResursa;
	}

	public void setDenumireResursa(String denumireResursa) {
		this.denumireResursa = denumireResursa;
	}

	public List<PeGrajd> getPegrajds() {
		return this.pegrajds;
	}

	public void setPegrajds(List<PeGrajd> pegrajds) {
		this.pegrajds = pegrajds;
	}

	public PeGrajd addPegrajd(PeGrajd pegrajd) {
		getPegrajds().add(pegrajd);
		pegrajd.setResurse(this);

		return pegrajd;
	}

	public PeGrajd removePegrajd(PeGrajd pegrajd) {
		getPegrajds().remove(pegrajd);
		pegrajd.setResurse(null);

		return pegrajd;
	}

	public List<PePasune> getPepasunes() {
		return this.pepasunes;
	}

	public void setPepasunes(List<PePasune> pepasunes) {
		this.pepasunes = pepasunes;
	}

	public PePasune addPepasune(PePasune pepasune) {
		getPepasunes().add(pepasune);
		pepasune.setResurse(this);

		return pepasune;
	}

	public PePasune removePepasune(PePasune pepasune) {
		getPepasunes().remove(pepasune);
		pepasune.setResurse(null);

		return pepasune;
	}

	public List<PeTeren> getPeterens() {
		return this.peterens;
	}

	public void setPeterens(List<PeTeren> peterens) {
		this.peterens = peterens;
	}

	public PeTeren addPeteren(PeTeren peteren) {
		getPeterens().add(peteren);
		peteren.setResurse(this);

		return peteren;
	}

	public PeTeren removePeteren(PeTeren peteren) {
		getPeterens().remove(peteren);
		peteren.setResurse(null);

		return peteren;
	}

	public Depozit getDepozitBean() {
		return this.depozitBean;
	}

	public void setDepozitBean(Depozit depozitBean) {
		this.depozitBean = depozitBean;
	}

	public Furnizor getFurnizorBean() {
		return this.furnizorBean;
	}

	public void setFurnizorBean(Furnizor furnizorBean) {
		this.furnizorBean = furnizorBean;
	}

	@Override
	public String toString() {
		return "Resurse [idResursa = " + idResursa + ", cantitate = " + cantitate + ", denumire = " + denumireResursa
				+ ", depozitBean = " + depozitBean + ", furnizorBean = " + furnizorBean + "]";
	}
}