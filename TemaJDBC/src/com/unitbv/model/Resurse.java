package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the resurse database table.
 * 
 */
@Entity
@NamedQuery(name="Resurse.findAll", query="SELECT r FROM Resurse r")
public class Resurse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_resursa")
	private int idResursa;

	private int cantitate;

	private String denumire;

	//bi-directional many-to-one association to Pegrajd
	@OneToMany(mappedBy="resurse")
	private List<Pegrajd> pegrajds;

	//bi-directional many-to-one association to Pepasune
	@OneToMany(mappedBy="resurse")
	private List<Pepasune> pepasunes;

	//bi-directional many-to-one association to Peteren
	@OneToMany(mappedBy="resurse")
	private List<Peteren> peterens;

	//bi-directional many-to-one association to Depozit
	@ManyToOne
	@JoinColumn(name="depozit")
	private Depozit depozitBean;

	//bi-directional many-to-one association to Furnizor
	@ManyToOne
	@JoinColumn(name="furnizor")
	private Furnizor furnizorBean;

	public Resurse() {
	}

	public Resurse(int idResursa, String denumire, int cantitate, Depozit depozitBean, Furnizor furnizorBean) {
		super();
		this.idResursa = idResursa;
		this.denumire = denumire;
		this.cantitate = cantitate;
		this.depozitBean = depozitBean;
		this.furnizorBean = furnizorBean;
	}

	public int getIdResursa() {
		return this.idResursa;
	}

	public void setIdResursa(int idResursa) {
		this.idResursa = idResursa;
	}

	public int getCantitate() {
		return this.cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public String getDenumire() {
		return this.denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public List<Pegrajd> getPegrajds() {
		return this.pegrajds;
	}

	public void setPegrajds(List<Pegrajd> pegrajds) {
		this.pegrajds = pegrajds;
	}

	public Pegrajd addPegrajd(Pegrajd pegrajd) {
		getPegrajds().add(pegrajd);
		pegrajd.setResurse(this);

		return pegrajd;
	}

	public Pegrajd removePegrajd(Pegrajd pegrajd) {
		getPegrajds().remove(pegrajd);
		pegrajd.setResurse(null);

		return pegrajd;
	}

	public List<Pepasune> getPepasunes() {
		return this.pepasunes;
	}

	public void setPepasunes(List<Pepasune> pepasunes) {
		this.pepasunes = pepasunes;
	}

	public Pepasune addPepasune(Pepasune pepasune) {
		getPepasunes().add(pepasune);
		pepasune.setResurse(this);

		return pepasune;
	}

	public Pepasune removePepasune(Pepasune pepasune) {
		getPepasunes().remove(pepasune);
		pepasune.setResurse(null);

		return pepasune;
	}

	public List<Peteren> getPeterens() {
		return this.peterens;
	}

	public void setPeterens(List<Peteren> peterens) {
		this.peterens = peterens;
	}

	public Peteren addPeteren(Peteren peteren) {
		getPeterens().add(peteren);
		peteren.setResurse(this);

		return peteren;
	}

	public Peteren removePeteren(Peteren peteren) {
		getPeterens().remove(peteren);
		peteren.setResurse(null);

		return peteren;
	}

	public Depozit getDepozitBean() {
		return this.depozitBean;
	}

	public void setDepozitBean(Depozit depozitBean) {
		this.depozitBean = depozitBean;
	}

	public Furnizor getFurnizorBean() {
		return this.furnizorBean;
	}

	public void setFurnizorBean(Furnizor furnizorBean) {
		this.furnizorBean = furnizorBean;
	}

	@Override
	public String toString() {
		return "Resurse [idResursa = " + idResursa + ", cantitate = " + cantitate + ", denumire = " + denumire
				+ ", depozitBean = " + depozitBean + ", furnizorBean = " + furnizorBean + "]";
	}

}