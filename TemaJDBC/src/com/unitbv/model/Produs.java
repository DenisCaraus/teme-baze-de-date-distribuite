package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the produs database table.
 * 
 */
@Entity
@NamedQuery(name="Produs.findAll", query="SELECT p FROM Produs p")
public class Produs implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_produs")
	private int idProdus;

	private int cantitate;

	private String denumire;

	private BigDecimal greutate;

	@Column(name="tip_produs")
	private String tipProdus;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="animal")
	private Animal animalBean;

	//bi-directional many-to-one association to Depozit
	@ManyToOne
	@JoinColumn(name="depozit")
	private Depozit depozitBean;

	//bi-directional many-to-one association to Distribuitor
	@ManyToOne
	@JoinColumn(name="distribuitor")
	private Distribuitor distribuitorBean;

	//bi-directional many-to-one association to Terenarabil
	@ManyToOne
	@JoinColumn(name="teren")
	private Terenarabil terenarabil;

	public Produs() {
	}

	public Produs(int idProdus, String tipProdus, String denumire, BigDecimal greutate, int cantitate,
			Terenarabil terenarabil, Animal animalBean, Depozit depozitBean, Distribuitor distribuitorBean) {
		super();
		this.idProdus = idProdus;
		this.tipProdus = tipProdus;
		this.denumire = denumire;
		this.greutate = greutate;
		this.cantitate = cantitate;
		this.terenarabil = terenarabil;
		this.animalBean = animalBean;
		this.depozitBean = depozitBean;
		this.distribuitorBean = distribuitorBean;
	}

	public int getIdProdus() {
		return this.idProdus;
	}

	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}

	public int getCantitate() {
		return this.cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public String getDenumire() {
		return this.denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public BigDecimal getGreutate() {
		return this.greutate;
	}

	public void setGreutate(BigDecimal greutate) {
		this.greutate = greutate;
	}

	public String getTipProdus() {
		return this.tipProdus;
	}

	public void setTipProdus(String tipProdus) {
		this.tipProdus = tipProdus;
	}

	public Animal getAnimalBean() {
		return this.animalBean;
	}

	public void setAnimalBean(Animal animalBean) {
		this.animalBean = animalBean;
	}

	public Depozit getDepozitBean() {
		return this.depozitBean;
	}

	public void setDepozitBean(Depozit depozitBean) {
		this.depozitBean = depozitBean;
	}

	public Distribuitor getDistribuitorBean() {
		return this.distribuitorBean;
	}

	public void setDistribuitorBean(Distribuitor distribuitorBean) {
		this.distribuitorBean = distribuitorBean;
	}

	public Terenarabil getTerenarabil() {
		return this.terenarabil;
	}

	public void setTerenarabil(Terenarabil terenarabil) {
		this.terenarabil = terenarabil;
	}

	@Override
	public String toString() {
		return "Produs [idProdus = " + idProdus + ", cantitate = " + cantitate + ", denumire = " + denumire + ", greutate = "
				+ greutate + ", tipProdus = " + tipProdus + ", animalBean = " + animalBean + ", depozitBean = " + depozitBean
				+ ", distribuitorBean = " + distribuitorBean + ", terenarabil = " + terenarabil + "]";
	}

}