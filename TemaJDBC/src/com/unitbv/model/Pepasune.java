package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pepasune database table.
 * 
 */
@Entity
@NamedQuery(name="Pepasune.findAll", query="SELECT p FROM Pepasune p")
public class Pepasune implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to Pasune
	@ManyToOne
	@JoinColumn(name="pasune")
	private Pasune pasuneBean;

	//bi-directional many-to-one association to Resurse
	@ManyToOne
	@JoinColumn(name="resursa")
	private Resurse resurse;

	public Pepasune() {
	}

	public Pepasune(int id, Pasune pasuneBean, Resurse resurse) {
		super();
		this.id = id;
		this.pasuneBean = pasuneBean;
		this.resurse = resurse;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pasune getPasuneBean() {
		return this.pasuneBean;
	}

	public void setPasuneBean(Pasune pasuneBean) {
		this.pasuneBean = pasuneBean;
	}

	public Resurse getResurse() {
		return this.resurse;
	}

	public void setResurse(Resurse resurse) {
		this.resurse = resurse;
	}

	@Override
	public String toString() {
		return "Pepasune [id = " + id + ", pasuneBean = " + pasuneBean + ", resurse = " + resurse + "]";
	}

}