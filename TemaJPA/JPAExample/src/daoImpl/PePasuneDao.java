package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.PePasune;

/**
 * DAO class for PePasune entity
 * 
 * @author k0bra
 *
 */
public class PePasuneDao implements Dao<PePasune> {

	private DatabaseHelper databaseHelper;

	public PePasuneDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<PePasune> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(PePasune.class, id));
	}

	@Override
	public List<PePasune> getAll() {
		TypedQuery<PePasune> query = databaseHelper.getEntityManager().createQuery("SELECT p from PePasune p",
				PePasune.class);
		return query.getResultList();
	}

	@Override
	public boolean create(PePasune pePasune) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(pePasune));
	}

	@Override
	public boolean update(PePasune old, PePasune newObj) {
		old.setId(newObj.getId());
		old.setPasuneBean(newObj.getPasuneBean());
		old.setResurse(newObj.getResurse());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(PePasune pePasune) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(pePasune));
	}

}