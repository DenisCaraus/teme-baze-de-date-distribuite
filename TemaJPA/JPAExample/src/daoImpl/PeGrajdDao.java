package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.PeGrajd;

/**
 * DAO class for PeGrajd entity
 * 
 * @author k0bra
 *
 */
public class PeGrajdDao implements Dao<PeGrajd> {

	private DatabaseHelper databaseHelper;

	public PeGrajdDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<PeGrajd> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(PeGrajd.class, id));
	}

	@Override
	public List<PeGrajd> getAll() {
		TypedQuery<PeGrajd> query = databaseHelper.getEntityManager().createQuery("SELECT p from PeGrajd p",
				PeGrajd.class);
		return query.getResultList();
	}

	@Override
	public boolean create(PeGrajd peGrajd) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(peGrajd));
	}

	@Override
	public boolean update(PeGrajd old, PeGrajd newObj) {
		old.setGrajdBean(newObj.getGrajdBean());
		old.setId(newObj.getId());
		old.setResurse(newObj.getResurse());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(PeGrajd peGrajd) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(peGrajd));
	}

}