package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Grajd;

/**
 * DAO class for Grajd entity
 * 
 * @author k0bra
 *
 */
public class GrajdDao implements Dao<Grajd> {

	private DatabaseHelper databaseHelper;

	public GrajdDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Grajd> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Grajd.class, id));
	}

	@Override
	public List<Grajd> getAll() {
		TypedQuery<Grajd> query = databaseHelper.getEntityManager().createQuery("SELECT g from Grajd g", Grajd.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Grajd grajd) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(grajd));
	}

	@Override
	public boolean update(Grajd old, Grajd newObj) {
		old.setAnimals(newObj.getAnimals());
		old.setCapacitateMaxima(newObj.getCapacitateMaxima());
		old.setMuncitors(newObj.getMuncitors());
		old.setPegrajds(newObj.getPegrajds());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Grajd grajd) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(grajd));
	}

}
