package dao;

import java.util.List;
import java.util.Optional;

/**
 * Generic interface for DAO purposes All CRUD options to be implemented in
 * classes.
 * 
 * @author Solo
 *
 * @param <T> Generic parameter
 */

public interface Dao<T> {

	/**
	 * Gets a specific entry from table <T>
	 * 
	 * @param id The id of the desired entry
	 * @return The desired entry if it exists
	 */
	Optional<T> get(int id);

	/**
	 * Gets all entries from table <T>
	 * 
	 * @return List with all entries
	 */
	List<T> getAll();

	/**
	 * Creates a new entry in table <T>
	 * 
	 * @param t The object that needs to be inserted in the <T> table
	 * @return True if the operation was successful, false otherwise
	 */
	boolean create(T t);

	/**
	 * Updates a specific entry from table <T>
	 * 
	 * @param old    The entry that needs updating
	 * @param newObj The new data
	 * @return True if the operation was successful, false otherwise
	 */
	boolean update(T old, T newObj);

	/**
	 * Deletes a specific entry from table <T>
	 * 
	 * @param t The entry that is to be deleted
	 * @return True if the operation was successful, false otherwise
	 */
	boolean delete(T t);
}
