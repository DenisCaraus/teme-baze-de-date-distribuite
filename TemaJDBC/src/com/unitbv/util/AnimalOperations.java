package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Animal;
import com.unitbv.model.Grajd;
import com.unitbv.model.Pasune;

public class AnimalOperations {

	private DatabaseConnection databaseConnection;

	public AnimalOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all animals entries from the database
	 * 
	 * @return The animal list
	 */
	public List<Animal> getAllAnimals() {
		databaseConnection.createConnection();
		String query = "SELECT idAnimal, tipAnimal, varsta, greutate, specie, idGrajd, capacitateMaxima, idPasune, arie "
				+ " FROM (animal LEFT JOIN grajd on animal.grajd = grajd.idGrajd) LEFT JOIN pasune on animal.pasune = pasune.idPasune";
		List<Animal> animals = new ArrayList<Animal>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Animal animal = new Animal();
				Pasune pasune = new Pasune();
				Grajd grajd = new Grajd();

				pasune.setIdPasune(rs.getInt("idPasune"));
				pasune.setArie(rs.getInt("arie"));

				grajd.setIdGrajd(rs.getInt("idGrajd"));
				grajd.setCapacitateMaxima(rs.getInt("capacitateMaxima"));

				animal.setIdAnimal(rs.getInt("idAnimal"));
				animal.setTipAnimal(rs.getString("tipAnimal"));
				animal.setVarsta(rs.getInt("varsta"));
				animal.setGreutate(rs.getBigDecimal("greutate"));
				animal.setSpecie(rs.getString("specie"));
				animal.setGrajdBean(grajd);
				animal.setPasuneBean(pasune);

				animals.add(animal);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return animals;
	}

	/**
	 * Adds a new animal entry to the database
	 * 
	 * @param animal The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addAnimal(Animal animal) {
		databaseConnection.createConnection();
		String query = "INSERT INTO animal VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, animal.getIdAnimal());
			ps.setString(2, animal.getTipAnimal());
			ps.setInt(3, animal.getVarsta());
			ps.setBigDecimal(4, animal.getGreutate());
			ps.setString(5, animal.getSpecie());
			ps.setInt(6, animal.getGrajdBean().getIdGrajd());
			ps.setInt(7, animal.getPasuneBean().getIdPasune());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an animal entry from the database
	 * 
	 * @param animal The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateAnimal(Animal animal) {
		databaseConnection.createConnection();
		String query = "UPDATE animal SET tipAnimal = ?, varsta = ?, greutate = ?, specie = ?, grajd = ?, pasune = ? WHERE idAnimal = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(7, animal.getIdAnimal());
			ps.setString(1, animal.getTipAnimal());
			ps.setInt(2, animal.getVarsta());
			ps.setBigDecimal(3, animal.getGreutate());
			ps.setString(4, animal.getSpecie());
			ps.setInt(5, animal.getGrajdBean().getIdGrajd());
			ps.setInt(6, animal.getPasuneBean().getIdPasune());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an animal entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteAnimal(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM animal where idAnimal = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
