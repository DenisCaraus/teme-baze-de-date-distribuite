package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Depozit;
import com.unitbv.model.Furnizor;
import com.unitbv.model.Resurse;

public class ResurseOperations {
	private DatabaseConnection databaseConnection;

	public ResurseOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all Resurse entries from the database
	 * 
	 * @return The Resurse list
	 */
	public List<Resurse> getAllResurse() {
		databaseConnection.createConnection();
		String query = "SELECT idResursa, denumireResursa, cantitate, idDepozit, tipDepozit, cantitateMaxima, idFurnizor, denumire "
				+ " FROM resurse LEFT JOIN depozit ON resurse.depozit = depozit.idDepozit LEFT JOIN furnizor ON resurse.furnizor = furnizor.idFurnizor";
		List<Resurse> resurse = new ArrayList<Resurse>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Resurse resursa = new Resurse();
				Depozit depozit = new Depozit();
				Furnizor furnizor = new Furnizor();

				depozit.setIdDepozit(rs.getInt("idDepozit"));
				depozit.setTipDepozit(rs.getString("tipDepozit"));
				depozit.setCantitateMaxima(rs.getInt("cantitateMaxima"));

				furnizor.setIdFurnizor(rs.getInt("idFurnizor"));
				furnizor.setDenumire(rs.getString("denumire"));

				resursa.setIdResursa(rs.getInt("idResursa"));
				resursa.setDenumire(rs.getString("denumireResursa"));
				resursa.setCantitate(rs.getInt("cantitate"));
				resursa.setDepozitBean(depozit);
				resursa.setFurnizorBean(furnizor);

				resurse.add(resursa);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return resurse;
	}

	/**
	 * Adds a new Resurse entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addResursa(Resurse resursa) {
		databaseConnection.createConnection();
		String query = "UPDATE resurse VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, resursa.getIdResursa());
			ps.setString(2, resursa.getDenumire());
			ps.setInt(3, resursa.getCantitate());
			ps.setInt(4, resursa.getDepozitBean().getIdDepozit());
			ps.setInt(5, resursa.getFurnizorBean().getIdFurnizor());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an Resurse entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */

	public boolean updateResursa(Resurse resursa) {
		databaseConnection.createConnection();
		String query = "UPDATE resurse SET denumireResursa = ?, cantitate = ?, depozit = ?, furnizor = ? WHERE idResursa = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(5, resursa.getIdResursa());
			ps.setString(1, resursa.getDenumire());
			ps.setInt(2, resursa.getCantitate());
			ps.setInt(3, resursa.getDepozitBean().getIdDepozit());
			ps.setInt(4, resursa.getFurnizorBean().getIdFurnizor());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an Resurse entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteResursa(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM resurse where idResursa = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
