package main;

import general.General;
import helper.DatabaseHelper;

public class Main {
	public static void main(String[] args) {
		DatabaseHelper dh = DatabaseHelper.getInstance();
		General.menu(dh);
	}
}
