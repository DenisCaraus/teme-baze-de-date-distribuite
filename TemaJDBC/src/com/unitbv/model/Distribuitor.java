package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the distribuitor database table.
 * 
 */
@Entity
@NamedQuery(name="Distribuitor.findAll", query="SELECT d FROM Distribuitor d")
public class Distribuitor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_distribuitor")
	private int idDistribuitor;

	private String denumire;

	@Column(name="suma_maxima_disponibila")
	private int sumaMaximaDisponibila;

	//bi-directional many-to-one association to Produs
	@OneToMany(mappedBy="distribuitorBean")
	private List<Produs> produses;

	public Distribuitor() {
	}

	public Distribuitor(int idDistribuitor, String denumire, int sumaMaximaDisponibila) {
		super();
		this.idDistribuitor = idDistribuitor;
		this.denumire = denumire;
		this.sumaMaximaDisponibila = sumaMaximaDisponibila;
	}

	public int getIdDistribuitor() {
		return this.idDistribuitor;
	}

	public void setIdDistribuitor(int idDistribuitor) {
		this.idDistribuitor = idDistribuitor;
	}

	public String getDenumire() {
		return this.denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public int getSumaMaximaDisponibila() {
		return this.sumaMaximaDisponibila;
	}

	public void setSumaMaximaDisponibila(int sumaMaximaDisponibila) {
		this.sumaMaximaDisponibila = sumaMaximaDisponibila;
	}

	public List<Produs> getProduses() {
		return this.produses;
	}

	public void setProduses(List<Produs> produses) {
		this.produses = produses;
	}

	public Produs addProdus(Produs produs) {
		getProduses().add(produs);
		produs.setDistribuitorBean(this);

		return produs;
	}

	public Produs removeProdus(Produs produs) {
		getProduses().remove(produs);
		produs.setDistribuitorBean(null);

		return produs;
	}

	@Override
	public String toString() {
		return "Distribuitor [idDistribuitor = " + idDistribuitor + ", denumire = " + denumire + ", sumaMaximaDisponibila = "
				+ sumaMaximaDisponibila + "]";
	}

}