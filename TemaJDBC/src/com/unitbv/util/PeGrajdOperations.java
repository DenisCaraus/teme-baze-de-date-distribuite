package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Depozit;
import com.unitbv.model.Furnizor;
import com.unitbv.model.Grajd;
import com.unitbv.model.Pegrajd;
import com.unitbv.model.Resurse;

public class PeGrajdOperations {
	private DatabaseConnection databaseConnection;

	public PeGrajdOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all PeGrajd entries from the database
	 * 
	 * @return The PeGrajd list
	 */
	public List<Pegrajd> getAllPeGrajd() {
		databaseConnection.createConnection();
		String query = "SELECT id, idGrajd, capacitateMaxima, idResursa, denumireResursa, cantitate, idDepozit, tipDepozit, cantitateMaxima, idFurnizor, denumire "
				+ " FROM pegrajd LEFT JOIN grajd ON pegrajd.grajd = grajd.idGrajd LEFT JOIN resurse ON pegrajd.resursa = resurse.idResursa LEFT JOIN depozit ON resurse.depozit = depozit.idDepozit LEFT JOIN furnizor ON resurse.furnizor = furnizor.idFurnizor";
		List<Pegrajd> peGrajduri = new ArrayList<Pegrajd>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Pegrajd peGrajd = new Pegrajd();
				Grajd grajd = new Grajd();
				Resurse resurse = new Resurse();
				Depozit depozit = new Depozit();
				Furnizor furnizor = new Furnizor();

				grajd.setIdGrajd(rs.getInt("idGrajd"));
				grajd.setCapacitateMaxima(rs.getInt("capacitateMaxima"));

				depozit.setIdDepozit(rs.getInt("idDepozit"));
				depozit.setTipDepozit(rs.getString("tipDepozit"));
				depozit.setCantitateMaxima(rs.getInt("cantitateMaxima"));

				furnizor.setIdFurnizor(rs.getInt("idFurnizor"));
				furnizor.setDenumire(rs.getString("denumire"));

				resurse.setIdResursa(rs.getInt("idResursa"));
				resurse.setDenumire(rs.getString("denumireResursa"));
				resurse.setCantitate(rs.getInt("cantitate"));
				resurse.setDepozitBean(depozit);
				resurse.setFurnizorBean(furnizor);

				peGrajd.setId(rs.getInt("id"));
				peGrajd.setGrajdBean(grajd);
				peGrajd.setResurse(resurse);

				peGrajduri.add(peGrajd);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return peGrajduri;
	}

	/**
	 * Adds a new PeGrajd entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addPeGrajd(Pegrajd peGrajd) {
		databaseConnection.createConnection();
		String query = "INSERT INTO pegrajd VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, peGrajd.getGrajdBean().getIdGrajd());
			ps.setInt(2, peGrajd.getResurse().getIdResursa());
			ps.setInt(3, peGrajd.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an PeGrajd entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updatePeGrajd(Pegrajd peGrajd) {
		databaseConnection.createConnection();
		String query = "UPDATE pegrajd SET grajd = ?, resursa = ? WHERE id = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, peGrajd.getGrajdBean().getIdGrajd());
			ps.setInt(2, peGrajd.getResurse().getIdResursa());
			ps.setInt(3, peGrajd.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an PeGrajd entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deletePeGrajd(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM pegrajd where id = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
