package com.unitbv.main;

import com.unitbv.database.Database;
import com.unitbv.database.DatabaseConnection;
import com.unitbv.util.General;

public class MainClass {
	public static void main(String[] args) {

		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/ferma", "root",
				"123456789");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		General.menu(databaseConnection);
	}
}
