package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.PeTeren;

/**
 * DAO class for PeTeren entity
 * 
 * @author k0bra
 *
 */
public class PeTerenDao implements Dao<PeTeren> {

	private DatabaseHelper databaseHelper;

	public PeTerenDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<PeTeren> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(PeTeren.class, id));
	}

	@Override
	public List<PeTeren> getAll() {
		TypedQuery<PeTeren> query = databaseHelper.getEntityManager().createQuery("SELECT p from PeTeren p",
				PeTeren.class);
		return query.getResultList();
	}

	@Override
	public boolean create(PeTeren peTeren) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(peTeren));
	}

	@Override
	public boolean update(PeTeren old, PeTeren newObj) {
		old.setResurse(newObj.getResurse());
		old.setTerenarabil(newObj.getTerenarabil());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(PeTeren peTeren) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(peTeren));
	}

}