package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the terenarabil database table.
 * 
 */
@Entity
@NamedQuery(name = "TerenArabil.findAll", query = "SELECT t FROM TerenArabil t")
public class TerenArabil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idTeren;

	private int arieArabila;

	private int cantitatePlantata;

	@Temporal(TemporalType.DATE)
	private Date dataCulegere;

	// bi-directional many-to-one association to Muncitor
	@OneToMany(mappedBy = "terenarabil")
	private List<Muncitor> muncitors;

	// bi-directional many-to-one association to PeTeren
	@OneToMany(mappedBy = "terenarabil")
	private List<PeTeren> peterens;

	// bi-directional many-to-one association to Produs
	@OneToMany(mappedBy = "terenarabil")
	private List<Produs> produses;

	public TerenArabil() {
	}

	public int getIdTeren() {
		return this.idTeren;
	}

	public void setIdTeren(int idTeren) {
		this.idTeren = idTeren;
	}

	public int getArieArabila() {
		return this.arieArabila;
	}

	public void setArieArabila(int arieArabila) {
		this.arieArabila = arieArabila;
	}

	public int getCantitatePlantata() {
		return this.cantitatePlantata;
	}

	public void setCantitatePlantata(int cantitatePlantata) {
		this.cantitatePlantata = cantitatePlantata;
	}

	public Date getDataCulegere() {
		return this.dataCulegere;
	}

	public void setDataCulegere(Date dataCulegere) {
		this.dataCulegere = dataCulegere;
	}

	public List<Muncitor> getMuncitors() {
		return this.muncitors;
	}

	public void setMuncitors(List<Muncitor> muncitors) {
		this.muncitors = muncitors;
	}

	public Muncitor addMuncitor(Muncitor muncitor) {
		getMuncitors().add(muncitor);
		muncitor.setTerenarabil(this);

		return muncitor;
	}

	public Muncitor removeMuncitor(Muncitor muncitor) {
		getMuncitors().remove(muncitor);
		muncitor.setTerenarabil(null);

		return muncitor;
	}

	public List<PeTeren> getPeterens() {
		return this.peterens;
	}

	public void setPeterens(List<PeTeren> peterens) {
		this.peterens = peterens;
	}

	public PeTeren addPeteren(PeTeren peteren) {
		getPeterens().add(peteren);
		peteren.setTerenarabil(this);

		return peteren;
	}

	public PeTeren removePeteren(PeTeren peteren) {
		getPeterens().remove(peteren);
		peteren.setTerenarabil(null);

		return peteren;
	}

	public List<Produs> getProduses() {
		return this.produses;
	}

	public void setProduses(List<Produs> produses) {
		this.produses = produses;
	}

	public Produs addProdus(Produs produs) {
		getProduses().add(produs);
		produs.setTerenarabil(this);

		return produs;
	}

	public Produs removeProdus(Produs produs) {
		getProduses().remove(produs);
		produs.setTerenarabil(null);

		return produs;
	}

	@Override
	public String toString() {
		return "Terenarabil [idTeren = " + idTeren + ", arie = " + arieArabila + ", cantitatePlantata = "
				+ cantitatePlantata + ", dataCulegere = " + dataCulegere + "]";
	}
}