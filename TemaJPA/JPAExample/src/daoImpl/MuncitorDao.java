package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Muncitor;

/**
 * DAO class for Muncitor entity
 * 
 * @author k0bra
 *
 */
public class MuncitorDao implements Dao<Muncitor> {

	private DatabaseHelper databaseHelper;

	public MuncitorDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Muncitor> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Muncitor.class, id));
	}

	@Override
	public List<Muncitor> getAll() {
		TypedQuery<Muncitor> query = databaseHelper.getEntityManager().createQuery("SELECT m from Muncitor m",
				Muncitor.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Muncitor muncitor) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(muncitor));
	}

	@Override
	public boolean update(Muncitor old, Muncitor newObj) {
		old.setGrajdBean(newObj.getGrajdBean());
		old.setNumePrenume(newObj.getNumePrenume());
		old.setPasuneBean(newObj.getPasuneBean());
		old.setSalariu(newObj.getSalariu());
		old.setTelefon(newObj.getTelefon());
		old.setTerenarabil(newObj.getTerenarabil());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Muncitor muncitor) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(muncitor));
	}

}