package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.TerenArabil;

/**
 * DAO class for TerenArabil entity
 * 
 * @author k0bra
 *
 */
public class TerenArabilDao implements Dao<TerenArabil> {

	private DatabaseHelper databaseHelper;

	public TerenArabilDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<TerenArabil> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(TerenArabil.class, id));
	}

	@Override
	public List<TerenArabil> getAll() {
		TypedQuery<TerenArabil> query = databaseHelper.getEntityManager().createQuery("SELECT t from TerenArabil t",
				TerenArabil.class);
		return query.getResultList();
	}

	@Override
	public boolean create(TerenArabil terenArabil) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(terenArabil));
	}

	@Override
	public boolean update(TerenArabil old, TerenArabil newObj) {
		old.setArieArabila(newObj.getArieArabila());
		old.setCantitatePlantata(newObj.getCantitatePlantata());
		old.setDataCulegere(newObj.getDataCulegere());
		old.setMuncitors(newObj.getMuncitors());
		old.setPeterens(newObj.getPeterens());
		old.setProduses(newObj.getProduses());

		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(TerenArabil terenArabil) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(terenArabil));
	}

}