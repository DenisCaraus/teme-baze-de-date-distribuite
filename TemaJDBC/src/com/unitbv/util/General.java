package com.unitbv.util;

import java.util.List;
import java.util.Scanner;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Furnizor;
import com.unitbv.model.Grajd;
import com.unitbv.model.Pasune;

public class General {

	/**
	 * Prints an list of elements
	 * 
	 * @param items
	 */
	public static <Type> void printList(List<Type> items) {
		Integer count = 0;
		for (Type item : items) {
			System.out.println(count.toString() + ". " + item.toString());
			count++;
		}
	}

	/**
	 * Demonstrative menu for this application
	 * 
	 * @param databaseConnection
	 */
	public static void menu(DatabaseConnection databaseConnection) {
		Scanner scan = new Scanner(System.in);
		int selection = 0;
		while (true) {
			System.out.println("Alege entitatea la care vrei sa lucrezi:");
			System.out.println("1.Pasune");
			System.out.println("2.Grajd");
			System.out.println("3.Furnizor");
			System.out.println("4.Afisarea tuturor animalelor, cu grajdurile si pasunile de care apartin");
			System.out.println("5.Exit");

			if (scan.hasNextInt())
				selection = scan.nextInt();

			switch (selection) {
			case 1:
				menuPasune(databaseConnection, scan);
				break;
			case 2:
				menuGrajd(databaseConnection, scan);
				break;
			case 3:
				menuFurnizor(databaseConnection, scan);
				break;	
			case 4:
				AnimalOperations animalOperations = new AnimalOperations(databaseConnection);
				General.printList(animalOperations.getAllAnimals());
				break;
			case 5:
				scan.close();
				return;
			}
		}
	}

	/**
	 * Demonstrative menu for Pasune CRUD
	 * 
	 * @param databaseConnection
	 */
	public static void menuPasune(DatabaseConnection databaseConnection, Scanner scan) {
		PasuneOperations pasuneOperations = new PasuneOperations(databaseConnection);

		System.out.println("Alege operatia:");
		System.out.println("1.Create");
		System.out.println("2.Read");
		System.out.println("3.Update");
		System.out.println("4.Delete");

		switch (scan.nextInt()) {
		case 1: {
			int id, arie;
			System.out.println("Introduceti id-ul");
			id = scan.nextInt();
			System.out.println("Introduceti aria");
			arie = scan.nextInt();
			Pasune pasune = new Pasune(id, arie);
			pasuneOperations.addPasune(pasune);
			break;
		}
		case 2: {
			printList(pasuneOperations.getAllPasunes());
			break;
		}
		case 3: {
			List<Pasune> list = pasuneOperations.getAllPasunes();
			printList(list);
			System.out.println("Alegeti itemul dupa numar.");
			Pasune pasune = list.get(scan.nextInt());
			System.out.println("Introduceti noua arie");
			pasune.setArie(scan.nextInt());
			pasuneOperations.updatePasune(pasune);
			break;
		}
		case 4: {
			List<Pasune> list = pasuneOperations.getAllPasunes();
			printList(list);
			System.out.println("Alegeti id-ul elementului care vreti sa fie sters.");
			pasuneOperations.deletePasune(scan.nextInt());
			break;
		}
		}

	}

	/**
	 * Demonstrative menu for Grajd CRUD
	 * 
	 * @param databaseConnection
	 */
	public static void menuGrajd(DatabaseConnection databaseConnection, Scanner scan) {
		GrajdOperations grajdOperations = new GrajdOperations(databaseConnection);

		System.out.println("Alege operatia:");
		System.out.println("1.Create");
		System.out.println("2.Read");
		System.out.println("3.Update");
		System.out.println("4.Delete");

		switch (scan.nextInt()) {
		case 1: {
			int id, arie;
			System.out.println("Introduceti id-ul");
			id = scan.nextInt();
			System.out.println("Introduceti aria");
			arie = scan.nextInt();
			Grajd grajd = new Grajd(id, arie);
			grajdOperations.addGrajd(grajd);
			break;
		}
		case 2: {
			printList(grajdOperations.getAllGrajd());
			break;
		}
		case 3: {
			List<Grajd> list = grajdOperations.getAllGrajd();
			printList(list);
			System.out.println("Alegeti itemul dupa numar.");
			Grajd grajd = list.get(scan.nextInt());
			System.out.println("Introduceti noua capacitate maxima");
			grajd.setCapacitateMaxima(scan.nextInt());
			grajdOperations.updateGrajd(grajd);
			break;
		}
		case 4: {
			List<Grajd> list = grajdOperations.getAllGrajd();
			printList(list);
			System.out.println("Alegeti id-ul elementului care vreti sa fie sters.");
			grajdOperations.deleteGrajd(scan.nextInt());
			break;
		}
		}

	}

	/**
	 * Demonstrative menu for Furnizor CRUD
	 * 
	 * @param databaseConnection
	 */
	public static void menuFurnizor(DatabaseConnection databaseConnection, Scanner scan) {
		FurnizorOperations furnizorOperations = new FurnizorOperations(databaseConnection);

		System.out.println("Alege operatia:");
		System.out.println("1.Create");
		System.out.println("2.Read");
		System.out.println("3.Update");
		System.out.println("4.Delete");

		switch (scan.nextInt()) {
		case 1: {
			int id;
			String denumire;
			System.out.println("Introduceti id-ul");
			id = scan.nextInt();
			System.out.println("Introduceti denumirea");
			denumire = scan.next();
			Furnizor pasune = new Furnizor(id, denumire);
			furnizorOperations.addFurnizor(pasune);
			break;
		}
		case 2: {
			printList(furnizorOperations.getAllFurnizors());
			break;
		}
		case 3: {
			List<Furnizor> list = furnizorOperations.getAllFurnizors();
			printList(list);
			System.out.println("Alegeti itemul dupa numar.");
			Furnizor furnizor = list.get(scan.nextInt());
			System.out.println("Introduceti noua denumire");
			furnizor.setDenumire(scan.nextLine());
			furnizorOperations.updateFurnizor(furnizor);
			break;
		}
		case 4: {
			List<Furnizor> list = furnizorOperations.getAllFurnizors();
			printList(list);
			System.out.println("Alegeti id-ul elementului care vreti sa fie sters.");
			furnizorOperations.deleteFurnizor(scan.nextInt());
			break;
		}
		}

	}
}
