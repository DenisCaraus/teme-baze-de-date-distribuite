package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Animal;
import com.unitbv.model.Depozit;
import com.unitbv.model.Distribuitor;
import com.unitbv.model.Grajd;
import com.unitbv.model.Pasune;
import com.unitbv.model.Produs;
import com.unitbv.model.Terenarabil;

public class ProdusOperations {
	private DatabaseConnection databaseConnection;

	public ProdusOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Obtains all Produs entries from the database
	 * 
	 * @return The Produs list
	 */
	public List<Produs> getAllProdus() {
		databaseConnection.createConnection();
		String query = "SELECT idProdus, tipProdus, denumireProdus, greutateProdus, cantitate, idTeren, dataCulegere, cantitatePlantata, arieArabila, idAnimal, tipAnimal, varsta, greutate, specie, idGrajd, capacitateMaxima, idPasune, arie, idDepozit, tipDepozit, cantitateMaxima, idDistribuitor, denumire, sumaMaximaDisponibila "
				+ " FROM produs LEFT JOIN terenarabil ON produs.teren = terenarabil.idTeren LEFT JOIN animal ON produs.animal = animal.idAnimal LEFT JOIN grajd ON animal.grajd = grajd.idGrajd LEFT JOIN pasune ON animal.pasune = pasune.idPasune LEFT JOIN depozit ON produs.depozit = depozit.idDepozit LEFT JOIN distribuitor ON produs.distribuitor = distribuitor.idDistribuitor";
		List<Produs> produse = new ArrayList<Produs>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Produs produs = new Produs();
				Terenarabil teren = new Terenarabil();
				Animal animal = new Animal();
				Pasune pasune = new Pasune();
				Grajd grajd = new Grajd();
				Depozit depozit = new Depozit();
				Distribuitor distribuitor = new Distribuitor();

				teren.setIdTeren(rs.getInt("idTeren"));
				teren.setCantitatePlantata(rs.getInt("cantitatePlantata"));
				teren.setArie(rs.getInt("arieArabila"));
				teren.setDataCulegere(rs.getDate("dataCulegere"));

				pasune.setIdPasune(rs.getInt("idPasune"));
				pasune.setArie(rs.getInt("arie"));

				grajd.setIdGrajd(rs.getInt("idGrajd"));
				grajd.setCapacitateMaxima(rs.getInt("capacitateMaxima"));

				animal.setIdAnimal(rs.getInt("idAnimal"));
				animal.setTipAnimal(rs.getString("tipAnimal"));
				animal.setVarsta(rs.getInt("varsta"));
				animal.setGreutate(rs.getBigDecimal("greutate"));
				animal.setSpecie(rs.getString("specie"));
				animal.setGrajdBean(grajd);
				animal.setPasuneBean(pasune);

				depozit.setIdDepozit(rs.getInt("idDepozit"));
				depozit.setTipDepozit(rs.getString("tipDepozit"));
				depozit.setCantitateMaxima(rs.getInt("cantitateMaxima"));

				distribuitor.setIdDistribuitor(rs.getInt("idDistribuitor"));
				distribuitor.setDenumire(rs.getString("denumire"));
				distribuitor.setSumaMaximaDisponibila(rs.getInt("sumaMaximaDisponibila"));

				produs.setIdProdus(rs.getInt("idProdus"));
				produs.setCantitate(rs.getInt("cantitate"));
				produs.setDenumire(rs.getString("denumireProdus"));
				produs.setGreutate(rs.getBigDecimal("greutateProdus"));
				produs.setTipProdus(rs.getString("tipProdus"));
				produs.setAnimalBean(animal);
				produs.setTerenarabil(teren);
				produs.setDistribuitorBean(distribuitor);
				produs.setDepozitBean(depozit);

				produse.add(produs);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return produse;
	}

	/**
	 * Adds a new Produs entry to the database
	 * 
	 * @param The entry to be added
	 * @return true if succesful false if not
	 */
	public boolean addProdus(Produs produs) {
		databaseConnection.createConnection();
		String query = "INSERT INTO produs VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, produs.getIdProdus());
			ps.setString(2, produs.getTipProdus());
			ps.setString(3, produs.getDenumire());
			ps.setBigDecimal(4, produs.getGreutate());
			ps.setInt(5, produs.getCantitate());
			ps.setInt(6, produs.getTerenarabil().getIdTeren());
			ps.setInt(7, produs.getAnimalBean().getIdAnimal());
			ps.setInt(8, produs.getDepozitBean().getIdDepozit());
			ps.setInt(9, produs.getDistribuitorBean().getIdDistribuitor());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Updates an Produs entry from the database
	 * 
	 * @param The entry to be updated
	 * @return true if succesful false if not
	 */
	public boolean updateProdus(Produs produs) {
		databaseConnection.createConnection();
		String query = "UPDATE produs SET tipProdus = ?, denumireProdus = ?, greutateProdus = ?, cantitate = ?, teren = ?, animal = ?, depozit = ?, distribuitor = ? WHERE idProdus = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(9, produs.getIdProdus());
			ps.setString(1, produs.getTipProdus());
			ps.setString(2, produs.getDenumire());
			ps.setBigDecimal(3, produs.getGreutate());
			ps.setInt(4, produs.getCantitate());
			ps.setInt(5, produs.getTerenarabil().getIdTeren());
			ps.setInt(6, produs.getAnimalBean().getIdAnimal());
			ps.setInt(7, produs.getDepozitBean().getIdDepozit());
			ps.setInt(8, produs.getDistribuitorBean().getIdDistribuitor());

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Deletes an Produs entry from the database based on its id
	 * 
	 * @param id The id of the entry to be deleted
	 * @return true if succesful false if not
	 */
	public boolean deleteProdus(int id) {

		databaseConnection.createConnection();
		String query = "DELETE FROM produs where idProdus = ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

}
