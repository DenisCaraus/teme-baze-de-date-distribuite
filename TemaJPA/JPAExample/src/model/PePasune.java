package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the pepasune database table.
 * 
 */
@Entity
@NamedQuery(name = "PePasune.findAll", query = "SELECT p FROM PePasune p")
public class PePasune implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	// bi-directional many-to-one association to Pasune
	@ManyToOne
	@JoinColumn(name = "pasune")
	private Pasune pasuneBean;

	// bi-directional many-to-one association to Resurse
	@ManyToOne
	@JoinColumn(name = "resursa")
	private Resurse resurse;

	public PePasune() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pasune getPasuneBean() {
		return this.pasuneBean;
	}

	public void setPasuneBean(Pasune pasuneBean) {
		this.pasuneBean = pasuneBean;
	}

	public Resurse getResurse() {
		return this.resurse;
	}

	public void setResurse(Resurse resurse) {
		this.resurse = resurse;
	}

	@Override
	public String toString() {
		return "Pepasune [id = " + id + ", pasuneBean = " + pasuneBean + ", resurse = " + resurse + "]";
	}
}